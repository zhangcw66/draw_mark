# draw_mark

> 一个基于ZRender(canvas)在图片上绘制标记；

> 画布在页面布局中居中显示，可拖拽、缩放、做标记。

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```
